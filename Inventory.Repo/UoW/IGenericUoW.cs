﻿using System;
using System.Data.Entity;
using Inventory.Repo.Repository;

namespace Inventory.Repo.UoW
{
    public interface IGenericUoW
    {
        IGenericRepository<T> GenericRepository<T>() where T : class;
        DbContextTransaction BeginTransaction();
        Int64 SaveChanges();
    }
}
