﻿using Inventory.Core.Model;
using Inventory.Repo.UoW;
using Inventory.Service.Interfaces;
using Inventory.Service.Services;
using Inventory.Core.ViewModel;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace Inventory.WebApi.Controllers
{
    [RoutePrefix("User")]
    public class UserController : ApiController
    {

        private readonly IUserSrvc _IUserSrvc;
        public UserController()
        {

            IGenericUoW _UoW = new GenericUoW();
            this._IUserSrvc = new UserSrvc(_UoW);
        }

        [Route("Register")]

        public HttpResponseMessage Register(VMRegisterUser RegisterUser)
        {
            _IUserSrvc.RegisterUser(RegisterUser);
            return Request.CreateResponse(HttpStatusCode.OK, RegisterUser);
        }


    }
    }