﻿using Inventory.Core.Model;
using Inventory.Repo.UoW;
using Inventory.Service.Interfaces;
using Inventory.Services.Interfaces;
using Inventory.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Inventory.WebApi.Controllers
{


    [RoutePrefix("Category")]
    public class CategoryController : ApiController
    {
        private readonly ICategorySrvc _ICategorySrvc;
        public CategoryController()
        {

            IGenericUoW _UoW = new GenericUoW();
            this._ICategorySrvc = new CategorySrvc(_UoW);
        }

        [HttpGet]
        [Route("GetAll")]
        public List<Category> GetAll()
        {
            return _ICategorySrvc.GetAll();

        }

    }
}
