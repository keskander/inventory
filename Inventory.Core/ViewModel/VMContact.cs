﻿using Inventory.Core.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Core.ViewModel
{
    public class VMContact : Contact
    {
        [Display(Name = "المبلغ")]
        public decimal PaymentValue { get; set; }

        [Display(Name = "تاريخ الدفع")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? PaymentDate { get; set; }

        [Display(Name = "تاريخ التجديد")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? NewExpierDate { get; set; }
    }
}
