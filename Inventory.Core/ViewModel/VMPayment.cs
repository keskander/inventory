﻿using Inventory.Core.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Core.ViewModel
{
    public class VMPayment : Payment
    {
        [Display(Name = "الاسم")]
        public string ContactName { get; set; }

        [Display(Name = "اسم البند")]
        public string RevExpItemName { get; set; }

    }
}
