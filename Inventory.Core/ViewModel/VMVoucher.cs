﻿using Inventory.Core.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Core.ViewModel
{
  
    public class VMVoucher : Voucher
    {
        public List<VMVoucherDetail> VoucherDetailList { get; set; }
        [Display(Name = "طريقه الدفع")]
        public string PaymentTypeID { get; set; }
        [Display(Name = "المبلغ")]
        public string PaymentValue { get; set; }

        [Display(Name = "رقم الشيك/البريد")]
        public string CheckNum { get; set; }
        [Display(Name = "الاسم")]
        public string ContactName { get; set; }
        [Display(Name = "الرصيد")]
        public decimal? ContactBalance { get; set; }
        [Display(Name = "دفع كاش")]
        public bool IsCashRetran { get; set; }
        [Display(Name = "تاريخ الدفع")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? PaymentDate { get; set; }

    }
}
