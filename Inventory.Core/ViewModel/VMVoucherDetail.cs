﻿using Inventory.Core.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Core.ViewModel
{
   public class VMVoucherDetail
    {
        public int? VoucherDetailID { get; set; }
        public int ItemID { get; set; }
        public int VoucherID { get; set; }
        [Display(Name = "تاريخ الانتهاء")]
        public System.DateTime ExpDate { get; set; }

        [Display(Name = "سعر التكلفه")]
        public decimal UnitCost { get; set; }

        [Display(Name = "سعر البيع")]
        public Nullable<decimal> Price { get; set; }
        [Display(Name = "التخفيض")]
        public Nullable<decimal> DiscountValue { get; set; }

        [Display(Name = "الكميه")]
        public decimal Quantity { get; set; }


        [Display(Name = "ك مرتجعه")]
        public decimal RetQuantity { get; set; }

        [Display(Name = "اجمالى")]
        public decimal Total { get; set; }
        [Display(Name = "المتاح")]
        public Nullable<decimal> Remanning { get; set; }

        public virtual Item Item { get; set; }
        public string RecveiveVoucherDetRef { get; set; }

   public decimal? PriceAfterDiscount { get; set; }
        [Display(Name = "الصنف")]
        public string ItemName { get; set; }
        [Display(Name = "الكود")]
        public string ItemCode { get; set; }
   
   
    }
}
