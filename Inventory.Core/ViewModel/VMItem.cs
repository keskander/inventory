﻿using Inventory.Core.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Core.ViewModel
{
    public class VMItem : Item
    {
 
        public List<Category> CategoriesLst { get; set; }
    }
}
