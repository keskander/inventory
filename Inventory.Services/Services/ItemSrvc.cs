﻿using Inventory.Service.Interfaces;
using Inventory.Repo.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inventory.Repo.UoW;
using Inventory.Core.Model;
using System.Text;
using Inventory.Core.ViewModel;
using InventoryUI.Services.Mapp;
using Inventory.Core.ViewModel;
using Inventory.Services.Interfaces;

namespace Inventory.Service.Services
{
    public class ItemSrvc : IItemSrvc
    {
        private readonly IGenericUoW _UoW;
        public ItemSrvc(IGenericUoW UoW)
        {
            _UoW = UoW;
        }
    
        public List<Item> Get()
        {
            return _UoW.GenericRepository<Item>().Get().ToList();
        }
    




        public List<Item> GetItemWithContactAndCategory(int takeNo)
        {
            return _UoW.GenericRepository<Item>().Get(x => x.isActive,null,x=>new { x.Category,x.Contact }).Take(takeNo).ToList();
         }

    
    }
}
