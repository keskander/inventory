﻿using Inventory.Core.Model;
using Inventory.Repo.UoW;
using Inventory.Service.Interfaces;
using Inventory.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Services.Services
{

     public class CategorySrvc : ICategorySrvc
    {
        private readonly IGenericUoW _UoW;
        public CategorySrvc(IGenericUoW UoW)
        {
            _UoW = UoW;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public void Insert(Category category)
        {
            _UoW.GenericRepository<Category>().Insert(category);
        }

        public List<Category> GetAll()
        {
            return _UoW.GenericRepository<Category>().Get(a=>a.isActive).ToList();
          
        }
    }
}
