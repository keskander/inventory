﻿using Inventory.Core.Model;
using Inventory.Repo.UoW;
using Inventory.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Services.Services
{

     public class VoucherSrvc : IVoucherSrvc
    {
        private readonly IGenericUoW _UoW;
        public VoucherSrvc(IGenericUoW UoW)
        {
            _UoW = UoW;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public void InsertVoucher(Voucher voucher)
        {
            _UoW.GenericRepository<Voucher>().Insert(voucher);
        }

        public Voucher GetVoucherByID(int ID)
        {
            Voucher voucher= _UoW.GenericRepository<Voucher>().Get(a => a.VoucherID == ID, null, x => x.Contact, x => x.VoucherDetails).FirstOrDefault();
            return voucher;
        }
    }
}
