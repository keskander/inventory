﻿using Inventory.Service.Interfaces;
using Inventory.Repo.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inventory.Repo.UoW;
using Inventory.Core.Model;
using System.Text;
using Inventory.Core.ViewModel;
using InventoryUI.Services.Mapp;
using Inventory.Core.ViewModel;

namespace Inventory.Service.Services
{
    public class UserSrvc : IUserSrvc
    {
        private readonly IGenericUoW _UoW;
        public UserSrvc(IGenericUoW UoW)
        {
            _UoW = UoW;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="PasswordHash"></param>
        /// <returns></returns>
        //public User ValidateUser(string Username, string PasswordHash)
        //{
        //    return _UoW.GenericRepository<User>().GetFirstOrDefault(x => x.Email == Username && x.Password == PasswordHash);
        //}
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<User> Get()
        {
            return _UoW.GenericRepository<User>().Get().ToList();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public Int64 InsertUserVoucher(User user, Voucher voucher)
        {
            Int64 uid = 0;
            using (var _dbtransaction = _UoW.BeginTransaction())
            {
                try
                {
                    _UoW.GenericRepository<User>().Insert(user);
                    _UoW.SaveChanges();
                    uid = user.Id;
                    //voucher.UserId = uid;

                    _UoW.GenericRepository<Voucher>().Insert(voucher);
                    _UoW.SaveChanges();
                    _dbtransaction.Commit();
                }
                catch (Exception)
                {
                    _dbtransaction.Rollback();
                }
                return uid;
            }
        }


     
        //public void Insert(Log log)
        //{
        //    _UoW.GenericRepository<Log>().Insert(log);
        //    _UoW.SaveChanges();
        //}


        public bool Login(string userName, string password)
        {
            try
            {

                var userInfo = _UoW.GenericRepository<User>().GetFirstOrDefault(x => x.UserName == userName && x.IsActive.Value);
                if (userInfo != null)
                {
                    string stringPwd = Encoding.ASCII.GetString(userInfo.Password).Trim('\0');
                    return stringPwd == password;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string GetUserRoles(string userName)
        {
            return _UoW.GenericRepository<User>().Get(x => x.UserName== userName,null,a=>a.UserRole).Select(x=>x.UserRole.UserRoleName).First();
        }

       public void RegisterUser(VMRegisterUser curUser)
        {

             _UoW.GenericRepository<User>().Insert(curUser.ConvertToUser());
            _UoW.SaveChanges();
        }
    
    }
}
