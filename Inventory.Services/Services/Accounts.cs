﻿using Inventory.Repo.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inventory.Service.Interfaces;
using Inventory.Repo.UoW;
using Inventory.Core.Model;


namespace Inventory.Services.Services
{
    public class Accounts
    {
        private readonly IGenericUoW _UoW;
        public Accounts(IGenericUoW UoW)
        {
            _UoW = UoW;
        }
        //public void Insert(Log log)
        //{
        //    _UoW.GenericRepository<Log>().Insert(log);
        //    _UoW.SaveChanges();
        //}


        public bool Login(string userName, string password)
        {
            try
            {

                var userInfo = _UoW.GenericRepository<User>().GetFirstOrDefault(x => x.UserName == userName);
                if (userInfo != null)
                {
                    string stringPwd = Encoding.ASCII.GetString(userInfo.Password);
                    return stringPwd == password;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        ////regitering new 
        //public bool Register(ApplicationUser userData)
        //{
        //    try
        //    {
        //        //register new user
        //        ApplicationDbContext.ApplicationUsers.Add(userData);
        //        ApplicationDbContext.SaveChanges();

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        ////To get user role provided with username
        //public string GetUserRole(string userName)
        //{
        //    return ApplicationDbContext.ApplicationUsers.Where(x => x.UserName == userName).Select(y => y.UserRole.RoleName).FirstOrDefault();
        //}

        //public List<string> GetUserRoles()
        //{
        //    return ApplicationDbContext.UserRoles.Select(x => x.RoleName).ToList();
        //}

    }
}
