﻿using Inventory.Service.Interfaces;
using Inventory.Repo.UoW;
using Inventory.Core.Model;

namespace OA.Service.Services
{
    public class OrderSrvc : IOrderSrvc
    {
        private readonly IGenericUoW _UoW;
        public OrderSrvc(IGenericUoW UoW)
        {
            _UoW = UoW;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public void InsertOrder(Voucher voucher)
        {
            _UoW.GenericRepository<Voucher>().Insert(voucher);
        }
    }
}
