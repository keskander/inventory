﻿using System;
using System.Collections.Generic;
using Inventory.Core.Model;
using Inventory.Core.ViewModel;

namespace Inventory.Service.Interfaces
{
    public interface IUserSrvc
    {
        //User ValidateUser(string Username, string PasswordHash);
        IEnumerable<User> Get();
        Int64 InsertUserVoucher(User user, Voucher voucher);
         bool Login(string userName, string password);
        string GetUserRoles(string userName);
        void RegisterUser(VMRegisterUser curUser);
    }
}
