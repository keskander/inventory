﻿using System;
using System.Collections.Generic;
using Inventory.Core.Model;
using Inventory.Core.ViewModel;

namespace Inventory.Service.Interfaces
{
    public interface ICategorySrvc
    {
        //User ValidateUser(string Username, string PasswordHash);
         List<Category> GetAll();
    }
}
