﻿using Inventory.Core.Model;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Services.Interfaces
{
   public interface IVoucherSrvc
    {
        void InsertVoucher(Voucher voucher);
         Voucher GetVoucherByID(int ID);

    }
}
