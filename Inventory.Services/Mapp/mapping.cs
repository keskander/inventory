﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Inventory.Core.ViewModel;
using Inventory.Core.Model;
using Inventory.Core.ViewModel;

namespace InventoryUI.Services.Mapp
{
    static class mapping
    {

        public static User ConvertToUser(this VMRegisterUser CurrUser)
        {
            User b = new User()
            {

                UserName = CurrUser.UserName,
                Password = Encoding.ASCII.GetBytes(CurrUser.Password),
                IsActive = CurrUser.IsActive
            };
            return b;


        }

        public static Voucher ConvertToVoucher(this VMVoucher CurrVoucher)
        {
            Voucher b = new Voucher()
            {
              
                insertDate = CurrVoucher.insertDate,
                ContactID = CurrVoucher.ContactID,
                //insertUser = null,
                InvoiceAmount = CurrVoucher.InvoiceAmount,
                InvoiceDate = CurrVoucher.InvoiceDate,
                InvoiceNo = CurrVoucher.InvoiceNo,
                IsActive = CurrVoucher.IsActive,
                PaymentRemaning = CurrVoucher.PaymentRemaning,
                ReceiptNo = CurrVoucher.ReceiptNo,
                VoucherID = CurrVoucher.VoucherID,
                VoucherTypeID = CurrVoucher.VoucherTypeID,
                RefVoucherID=CurrVoucher.RefVoucherID,
                VouucherDiscount = CurrVoucher.VouucherDiscount,
                Payments = ( CurrVoucher.PaymentValue != null) ? new List<Payment>() { new Payment {VoucherID= CurrVoucher.VoucherID, PaymentTypeID = CurrVoucher.PaymentTypeID!=null? int.Parse(CurrVoucher.PaymentTypeID):1, PaymentValue = decimal.Parse(CurrVoucher.PaymentValue), CheckNum = CurrVoucher.CheckNum, PaymentDate = CurrVoucher.PaymentDate.HasValue? CurrVoucher.PaymentDate.Value: DateTime.Now } } : null,
                VoucherDetails =( CurrVoucher.VoucherDetailList!=null && CurrVoucher.VoucherDetailList.Count > 0) ?  CurrVoucher.VoucherDetailList.Select(a => a.ConvertToVoucherDetail()).ToList()  : null

            };
            return b;


        }
        public static VMVoucher ConvertToVMVoucher(this Voucher CurrVoucher)
        {
            VMVoucher b = new VMVoucher()
            {

                insertDate = CurrVoucher.insertDate,
                ContactID = CurrVoucher.ContactID,
                ContactName = CurrVoucher.Contact.ContactName,
                ContactBalance = CurrVoucher.Contact.Balance,
               
                InvoiceAmount = CurrVoucher.InvoiceAmount,
                InvoiceDate = CurrVoucher.InvoiceDate,
                InvoiceNo = CurrVoucher.InvoiceNo,
                IsActive = CurrVoucher.IsActive,
                PaymentRemaning = CurrVoucher.PaymentRemaning,
                ReceiptNo = CurrVoucher.ReceiptNo,
                VoucherID = CurrVoucher.VoucherID,
                VoucherTypeID = CurrVoucher.VoucherTypeID,
                VouucherDiscount = CurrVoucher.VouucherDiscount,

                ////Payments = (CurrVoucher.PaymentTypeID != null && CurrVoucher.PaymentValue != null) ? new List<Payment>() { new Payment { PaymentTypeID = int.Parse(CurrVoucher.PaymentTypeID), PaymentValue = decimal.Parse(CurrVoucher.PaymentValue), CheckNum = CurrVoucher.CheckNum, PaymentDate = DateTime.Now } } : null,
                //PaymentTypeID = CurrVoucher.Payments.FirstOrDefault().PaymentTypeID.ToString(),
                //PaymentValue = CurrVoucher.Payments.FirstOrDefault().PaymentValue.ToString(),
                //CheckNum= CurrVoucher.Payments.FirstOrDefault().CheckNum.ToString(),
                ////PaymentDate = CurrVoucher.Payments.FirstOrDefault().PaymentDate.ToString(),
                VoucherDetailList = CurrVoucher.VoucherDetails.Count > 0 ? CurrVoucher.VoucherDetails.Select(a => a.ConvertToVMVoucherDetail()).ToList() : null
            };
            return b;


        }

        public static VMContact ConvertToVMContact(this Contact CurrContact)
        {
            VMContact b = new VMContact()
            {
                Balance = CurrContact.Balance,
                ContactID = CurrContact.ContactID,
                ContactName = CurrContact.ContactName,
                ContactAddress = CurrContact.ContactAddress,
                ContactCode = CurrContact.ContactCode,
                ContactMobile = CurrContact.ContactMobile,
                ContactTel = CurrContact.ContactTel,
                IsActive = CurrContact.IsActive,
                ContactType = CurrContact.ContactType,
                ContactTypeID = CurrContact.ContactTypeID,
                ExpireDate = CurrContact.ExpireDate,
                DiscountPercentage = CurrContact.DiscountPercentage,
                OpenBalance = CurrContact.OpenBalance,
            };
            return b;
        }


        public static Contact ConvertToContact(this VMContact CurrContact)
        {
            Contact b = new Contact()
            {
                Balance = CurrContact.Balance,
                ContactID = CurrContact.ContactID,
                ContactName = CurrContact.ContactName,
                ContactAddress = CurrContact.ContactAddress,
                ContactCode = CurrContact.ContactCode,
                ContactMobile = CurrContact.ContactMobile,
                ContactTel = CurrContact.ContactTel,
                IsActive = CurrContact.IsActive,
                ContactType = CurrContact.ContactType,
                ContactTypeID = CurrContact.ContactTypeID,
                ExpireDate = CurrContact.ExpireDate,
                DiscountPercentage = CurrContact.DiscountPercentage,
                OpenBalance = CurrContact.OpenBalance,
            };
            return b;
        }


        public static VMPayment ConvertToVMPayment(this Payment CurPayment)
        {
            VMPayment b = new VMPayment()
            {
                PaymentCategoryID = CurPayment.PaymentCategoryID,
                CheckNum = CurPayment.CheckNum,
               ContactID = CurPayment.ContactID,
                ContactName = CurPayment.Contact!=null? CurPayment.Contact.ContactName:"",
                DueDate = CurPayment.DueDate,
                insertedBy = CurPayment.insertedBy,
                insertedDate = CurPayment.insertedDate,
                Notes = CurPayment.Notes,
                PaymentDate = CurPayment.PaymentDate,
                PaymentID = CurPayment.PaymentID,
                IsActive = CurPayment.IsActive,
                PaymentTypeID = CurPayment.PaymentTypeID,
                PaymentValue = CurPayment.PaymentValue,
                RevExpItemName = CurPayment.RevExpItem!=null? CurPayment.RevExpItem.RevExpItemName:"",
                RevExpItemID = CurPayment.RevExpItemID,
                VoucherID = CurPayment.VoucherID


            };
            return b;
        }


        public static Payment ConvertToPayment(this VMPayment CurPayment)
        {
            Payment b = new Payment()
            {
                PaymentCategoryID = CurPayment.PaymentCategoryID,
                CheckNum = CurPayment.CheckNum,
                ContactID = CurPayment.ContactID,
                DueDate = CurPayment.DueDate,
                insertedBy = CurPayment.insertedBy,
                insertedDate = CurPayment.insertedDate,
                Notes = CurPayment.Notes,
                PaymentDate = CurPayment.PaymentDate,
                PaymentID = CurPayment.PaymentID,
                IsActive = CurPayment.IsActive,
                PaymentTypeID = CurPayment.PaymentTypeID,
                PaymentValue = CurPayment.PaymentValue,
                RevExpItemID = CurPayment.RevExpItemID,
                VoucherID = CurPayment.VoucherID

            };
            return b;
        }



        public static VoucherDetail ConvertToVoucherDetail(this VMVoucherDetail CurrVoucherDetail)
        {
            VoucherDetail b = new VoucherDetail()
            {
                VoucherDetailID = CurrVoucherDetail.VoucherDetailID.HasValue? CurrVoucherDetail.VoucherDetailID.Value:0,
                DiscountValue = CurrVoucherDetail.DiscountValue,
                ItemID = CurrVoucherDetail.ItemID,
              UnitCost= CurrVoucherDetail.UnitCost,
                Price = CurrVoucherDetail.Price,
                PriceAfterDiscount = CurrVoucherDetail.Price- (CurrVoucherDetail.DiscountValue/ CurrVoucherDetail.Quantity),
                Quantity = CurrVoucherDetail.Quantity ,
                Total= CurrVoucherDetail.Total
            };
            return b;


        }


    public static VMVoucherDetail ConvertToVMVoucherDetail(this VoucherDetail CurrVoucherDetail)
        {
            VMVoucherDetail b = new VMVoucherDetail()
            {
                VoucherDetailID = CurrVoucherDetail.VoucherDetailID,
                DiscountValue = CurrVoucherDetail.DiscountValue,
               UnitCost= CurrVoucherDetail.UnitCost,
                ItemID = CurrVoucherDetail.ItemID,
                Price= CurrVoucherDetail.Price,
                Quantity= CurrVoucherDetail.Quantity ,
                PriceAfterDiscount= CurrVoucherDetail.PriceAfterDiscount,
                Total = CurrVoucherDetail.Total,
                ItemName= CurrVoucherDetail.Item.ItemName,
                ItemCode= CurrVoucherDetail.Item.ItemCode,
            };
            return b;


        }
      

    }
}

