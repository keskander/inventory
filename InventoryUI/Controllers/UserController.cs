﻿using Inventory.Core.Model;
using Inventory.Core.ViewModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace InventoryUI.Controllers
{
    
    public class UserController : Controller
    {
        string ApiPath = System.Configuration.ConfigurationManager.AppSettings["ApiPath"];


        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //// GET: User/Edit/5
        //public ActionResult Login()
        //{
     
        //    return View();
        //}
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Login(User model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }

        //    // This doesn't count login failures towards account lockout
        //    // To enable password failures to trigger account lockout, change to shouldLockout: true
        //    var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
        //    switch (result)
        //    {
        //        case SignInStatus.Success:
        //            return RedirectToAction("index","Home");


        //        default:
        //            ModelState.AddModelError("", "Invalid login attempt.");
        //            return View(model);
        //    }
        //}

     public ActionResult Register()
        {
     
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(User model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

      
            var jsonRequest = JsonConvert.SerializeObject(model);
            var content = new StringContent(jsonRequest, Encoding.UTF8, "text/json");
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ApiPath);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.PostAsync("User/Register", content);
                if (Res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index","Home");
                    //var ArtResponse = Res.Content.ReadAsStringAsync().Result;
                    //return Json(ModelState.IsValid ? true : ModelState.ToDataSourceResult());
                }

            }
            return View(model);
        }

        public ActionResult Login()
        {

            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(VMUser model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            using (var client = new HttpClient())
            {
                //setup client
                client.BaseAddress = new Uri(ApiPath);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //setup login data
                var formContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username",model.UserName),
                    new KeyValuePair<string, string>("password", model.Password),
                });

                HttpResponseMessage responseMessage = await client.PostAsync("/Token", formContent);
         
                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseJson = await responseMessage.Content.ReadAsStringAsync();
                    var jObject = JObject.Parse(responseJson);
                    Session["access_token"] = jObject.GetValue("access_token").ToString();
                    Session["UserName"] = model.UserName;
                    return RedirectToAction("Index", "Home");
                    //var ArtResponse = Res.Content.ReadAsStringAsync().Result;
                    //return Json(ModelState.IsValid ? true : ModelState.ToDataSourceResult());
                }

           
                return View(model);
            }

   
           
        }

    }
}
