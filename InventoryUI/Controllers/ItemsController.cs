﻿
using Inventory.Core.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

using System.Web.Mvc;

namespace InventoryUI.Controllers
{
    public class ItemsController : Controller
    {

        string ApiPath = System.Configuration.ConfigurationManager.AppSettings["ApiPath"];


        public ActionResult Index(int? id)
        {
            ViewBag.category = getCategory(); 
            //var includes = new Includes<Item>(query =>
            //{
            //    return query.Include(b => b.Category)
            //                    .Include(a => a.Contact);
            //});
            Item curItem=new Item() { ItemCode="0"};
           
            //var curItem = ItemRepositry.Include(a=>a.Contact).Where(a=>a.ItemID==id).FirstOrDefault();
            //if (id.HasValue)
            //curItem= ItemRepositry.FindBy(a => a.ItemID ==id.Value, null, includes.Expression).FirstOrDefault(a=>a.isActive);
            //if (TempData["CurItem"] != null)
            //    curItem =(Item) TempData["CurItem"];
            return View(curItem);
        }
        public async Task<ActionResult>  GetGridDetails(bool AllData)
        {
            List<Item> itemList = null;
            int itemNo = AllData == true ? 200000 : 30;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ApiPath);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage responseMessage = await client.GetAsync("/Category/GetAll?takeNo=" + itemNo.ToString());
            
            if (responseMessage.IsSuccessStatusCode)
            {
                //Storing the response details recieved from web api   
                var Response = responseMessage.Content.ReadAsStringAsync().Result;

                //Deserializing the response recieved from web api and storing into the Employee list  
                itemList = JsonConvert.DeserializeObject<List<Item>>(Response);

            }
            }
            return PartialView(itemList);
        }


        private async Task<List<Category>> getCategory()
        {

            if (HttpContext.Application["CategoryLst"] != null)
            {
                return ( List<Category>)HttpContext.Application["CategoryLst"];

            }

           
            using (var client = new HttpClient())
            {
                //setup client
                client.BaseAddress = new Uri(ApiPath);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                List<Category> CategoryLst = null;


                HttpResponseMessage responseMessage = await client.GetAsync("/Category/GetAll");

                if (responseMessage.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var Response = responseMessage.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    CategoryLst = JsonConvert.DeserializeObject<List<Category>>(Response);

                }

                return CategoryLst;


            }
        }

        //public ActionResult GetGridDetails(bool AllData)
        //{
        //    var includes = new Includes<Item>(query =>
        //    {
        //        return query.Include(b => b.Category)
        //                        .Include(a => a.Contact);
        //    });

        //    List<Item> itemList = null;
        //    if (AllData == true)
        //        itemList = ItemRepositry.FindBy(a => a.isActive,null, includes.Expression).OrderByDescending(b => b.ItemID).ToList();
        //    else
        //        itemList = ItemRepositry.FindBy(a => a.isActive,null, includes.Expression).OrderByDescending(b => b.ItemID).Take(20).ToList(); 

        //    return PartialView(itemList);
        //}



        //public int Delete(int id)
        //{
        //    var curItem = ItemRepositry.FindBy(s => s.ItemID == id).FirstOrDefault();
        //    curItem.isActive = false;
        //    ItemRepositry.Update(curItem);
        //    return 1;


        //}
        //[HttpPost]
        //public ActionResult Save(Item CurItem)
        //{
        //    CurItem.Contact = null;

        //    if (ModelState.IsValid)
        //    {

        //        if (CurItem.ItemID == 0)
        //        {
        //            CurItem.isActive = true;
        //            int c =ItemRepositry.FindBy(a => a.ItemName == CurItem.ItemName).Count();
        //            if (c != 0)
        //            {
        //                TempData["CurItem"] = CurItem;
        //                TempData["error"] = "هذا الاسم موجود من قبل";
        //                return RedirectToAction("Index");
        //            }
        //             c = ItemRepositry.FindBy(a => a.ItemCode == CurItem.ItemCode).Count();
        //            if (c != 0)
        //            {
        //                TempData["CurItem"] = CurItem;
        //                TempData["error"] = "هذا الكود موجود من قبل";
        //                return RedirectToAction("Index");
        //            }
        //        }

        //        if (CurItem.ItemID == 0)
        //            CurItem.isActive = true;
        //        //CurItem.CategoryID = 1;
        //        if( CurItem.ItemCode=="0")
        //            CurItem.ItemCode=GenerateItemCode();
        //        CurItem.CreationDate = DateTime.Now;
        //        CurItem.CreatedBy = (int)Session["UserId"];
        //        ItemRepositry.Save(CurItem);
        //    }
        //    else
        //    { var errors = ModelState.Values.SelectMany(vd => vd.Errors); }

        //   return RedirectToAction("Index");
        //}

        //private string GenerateItemCode()
        //{
        //  return(  (ItemRepositry.GetAll().ToList().Max(a => decimal.Parse(a.ItemCode))+1).ToString());
        //}

        //[HttpPost]
        //public JsonResult AutoCompleteItemName(string prefix)
        //{
        //    var itemList = ItemRepositry.FindBy(a=>a.isActive).ToList();
        //    if (prefix != "*")
        //        itemList = ItemRepositry.FindBy(a => a.ItemName.Contains(prefix) == true).OrderBy(s => s.ItemCode).ToList();

        //    return Json(itemList.Select(a => new
        //    {
        //        label = a.ItemName,
        //        val = a.ItemID
        //    }), JsonRequestBehavior.AllowGet);
        //}
        //[HttpPost]
        //public JsonResult AutoCompleteItemCode(string prefix)
        //{
        //    var itemList = ItemRepositry.FindBy(a => a.isActive).ToList();
        //    if (prefix != "*")
        //        itemList = ItemRepositry.FindBy(a => a.ItemCode.StartsWith(prefix) == true).OrderBy(s=>s.ItemCode).ToList();

        //    return Json(itemList.Select(a => new
        //    {
        //        label = a.ItemName,
        //        val = a.ItemID
        //    }), JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult GetItemData(string ItemCode,string ItemID)
        //{
        //    dynamic itemList ;
        //    if (ItemCode != "")
        //        itemList = ItemRepositry.FindBy(a => a.ItemCode == ItemCode).FirstOrDefault();
        //    else
        //        itemList = ItemRepositry.FindBy(a => a.ItemID == int.Parse(ItemID)).FirstOrDefault();
        //    return Json(itemList, JsonRequestBehavior.AllowGet);
        //}
    }
}